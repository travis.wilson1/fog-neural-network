{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "oYM61xrTsP5d"
   },
   "source": [
    "# TF Hub for TF2: Retraining an image classifier\n",
    "\n",
    "<table align=\"left\">\n",
    "<td align=\"center\">\n",
    "  <a target=\"_blank\"  href=\"https://colab.research.google.com/github/tensorflow/hub/blob/master/examples/colab/tf2_image_retraining.ipynb\">\n",
    "    <img src=\"https://www.tensorflow.org/images/colab_logo_32px.png\" /><br>Run in Google Colab\n",
    "  </a>\n",
    "</td>\n",
    "<td align=\"center\">\n",
    "  <a target=\"_blank\"  href=\"https://github.com/tensorflow/hub/blob/master/examples/colab/tf2_image_retraining.ipynb\">\n",
    "    <img width=32px src=\"https://www.tensorflow.org/images/GitHub-Mark-32px.png\" /><br>View source on GitHub</a>\n",
    "</td>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "L1otmJgmbahf"
   },
   "source": [
    "## Introduction\n",
    "\n",
    "Image classification models have millions of parameters. Training them from\n",
    "scratch requires a lot of labeled training data and a lot of computing power. Transfer learning is a technique that shortcuts much of this by taking a piece of a model that has already been trained on a related task and reusing it in a new model.\n",
    "\n",
    "This Colab demonstrates how to build a Keras model for classifying five species of flowers by using a pre-trained TF2 SavedModel from TensorFlow Hub for image feature extraction, trained on the much larger and more general ImageNet dataset. Optionally, the feature extractor can be trained (\"fine-tuned\") alongside the newly added classifier.\n",
    "\n",
    "### Looking for a tool instead?\n",
    "\n",
    "This is a TensorFlow coding tutorial. If you want a tool that just builds the TensorFlow or TF Lite model for, take a look at the [make_image_classifier](https://github.com/tensorflow/hub/tree/master/tensorflow_hub/tools/make_image_classifier) command-line tool that gets [installed](https://www.tensorflow.org/hub/installation) by the PIP package `tensorflow-hub[make_image_classifier]`, or at [this](https://colab.sandbox.google.com/github/tensorflow/examples/blob/master/tensorflow_examples/lite/model_customization/demo/image_classification.ipynb) TF Lite colab.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "bL54LWCHt5q5"
   },
   "source": [
    "## Set up TensorFlow 2 and other libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "txdrFJrJvgQS"
   },
   "outputs": [],
   "source": [
    "try:\n",
    "  # %tensorflow_version only exists in Colab.\n",
    "  %tensorflow_version 2.x\n",
    "except Exception:\n",
    "  pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "dlauq-4FWGZM"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "TF version: 2.0.0-rc0\n",
      "Hub version: 0.7.0\n",
      "GPU is NOT AVAILABLE\n"
     ]
    }
   ],
   "source": [
    "import itertools\n",
    "import os\n",
    "\n",
    "import matplotlib.pylab as plt\n",
    "import numpy as np\n",
    "\n",
    "import tensorflow as tf\n",
    "import tensorflow_hub as hub\n",
    "\n",
    "\n",
    "print(\"TF version:\", tf.__version__)\n",
    "print(\"Hub version:\", hub.__version__)\n",
    "print(\"GPU is\", \"available\" if tf.test.is_gpu_available() else \"NOT AVAILABLE\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "mmaHHH7Pvmth"
   },
   "source": [
    "## Select the TF2 SavedModel module to use\n",
    "\n",
    "For starters, use https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/4. The same URL can be used in code to identify the SavedModel and in your browser to show its documentation. (Note that `hub.Modules` for TF 1.x won't work here.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "FlsEcKVeuCnf"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/4 with input size (224, 224)\n"
     ]
    }
   ],
   "source": [
    "module_selection = (\"mobilenet_v2_100_224\", 224) #@param [\"(\\\"mobilenet_v2_100_224\\\", 224)\", \"(\\\"inception_v3\\\", 299)\"] {type:\"raw\", allow-input: true}\n",
    "handle_base, pixels = module_selection\n",
    "MODULE_HANDLE =\"https://tfhub.dev/google/imagenet/{}/feature_vector/4\".format(handle_base)\n",
    "IMAGE_SIZE = (pixels, pixels)\n",
    "print(\"Using {} with input size {}\".format(MODULE_HANDLE, IMAGE_SIZE))\n",
    "\n",
    "BATCH_SIZE = 32 #@param {type:\"integer\"}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "yTY8qzyYv3vl"
   },
   "source": [
    "## Set up the dataset\n",
    "\n",
    "Inputs are suitably resized for the selected module. Dataset augmentation (i.e., random distortions of an image each time it is read) improves training, esp. when fine-tuning."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "WBtFK1hO8KsO"
   },
   "outputs": [],
   "source": [
    "data_dir ='/Users/Travis/Programs/Air/python3/tensorflow2/transferlearning_image_classification/webcams/webcam_photos'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "umB5tswsfTEQ"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Found 358 images belonging to 2 classes.\n",
      "Found 1437 images belonging to 2 classes.\n"
     ]
    }
   ],
   "source": [
    "datagen_kwargs = dict(rescale=1./255, validation_split=.20)\n",
    "dataflow_kwargs = dict(target_size=IMAGE_SIZE, batch_size=BATCH_SIZE,\n",
    "                   interpolation=\"bilinear\")\n",
    "\n",
    "valid_datagen = tf.keras.preprocessing.image.ImageDataGenerator(\n",
    "    **datagen_kwargs)\n",
    "valid_generator = valid_datagen.flow_from_directory(\n",
    "    data_dir, subset=\"validation\", shuffle=False, **dataflow_kwargs)\n",
    "\n",
    "do_data_augmentation = True #@param {type:\"boolean\"}\n",
    "if do_data_augmentation:\n",
    "  train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(\n",
    "      rotation_range=40,\n",
    "      horizontal_flip=True,\n",
    "      width_shift_range=0.2, height_shift_range=0.2,\n",
    "      shear_range=0.2, zoom_range=0.2,\n",
    "      **datagen_kwargs)\n",
    "else:\n",
    "  train_datagen = valid_datagen\n",
    "train_generator = train_datagen.flow_from_directory(\n",
    "    data_dir, subset=\"training\", shuffle=True, **dataflow_kwargs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "FS_gVStowW3G"
   },
   "source": [
    "\n",
    "## Defining the model\n",
    "\n",
    "All it takes is to put a linear classifier on top of the `feature_extractor_layer` with the Hub module.\n",
    "\n",
    "For speed, we start out with a non-trainable `feature_extractor_layer`, but you can also enable fine-tuning for greater accuracy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "RaJW3XrPyFiF"
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "50FYNIb1dmJH"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Building model with https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/4\n",
      "Model: \"sequential\"\n",
      "_________________________________________________________________\n",
      "Layer (type)                 Output Shape              Param #   \n",
      "=================================================================\n",
      "keras_layer (KerasLayer)     multiple                  2257984   \n",
      "_________________________________________________________________\n",
      "dropout (Dropout)            multiple                  0         \n",
      "_________________________________________________________________\n",
      "dense (Dense)                multiple                  2562      \n",
      "=================================================================\n",
      "Total params: 2,260,546\n",
      "Trainable params: 2,226,434\n",
      "Non-trainable params: 34,112\n",
      "_________________________________________________________________\n"
     ]
    }
   ],
   "source": [
    "print(\"Building model with\", MODULE_HANDLE)\n",
    "model = tf.keras.Sequential([\n",
    "    hub.KerasLayer(MODULE_HANDLE, trainable=True),\n",
    "    tf.keras.layers.Dropout(rate=0.2),\n",
    "    tf.keras.layers.Dense(train_generator.num_classes, activation='softmax',\n",
    "                          kernel_regularizer=tf.keras.regularizers.l2(0.0001))\n",
    "])\n",
    "model.build((None,)+IMAGE_SIZE+(3,))\n",
    "model.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "u2e5WupIw2N2"
   },
   "source": [
    "## Training the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "9f3yBUvkd_VJ"
   },
   "outputs": [],
   "source": [
    "model.compile(\n",
    "  optimizer=tf.keras.optimizers.SGD(lr=0.005, momentum=0.9), \n",
    "  loss=tf.keras.losses.CategoricalCrossentropy(label_smoothing=0.1),\n",
    "  metrics=['accuracy'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "w_YKX2Qnfg6x"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch 1/15\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "WARNING: Logging before flag parsing goes to stderr.\n",
      "W0811 16:05:48.372974 4492303808 deprecation.py:323] From /Users/Travis/Environments/py3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_grad.py:1394: where (from tensorflow.python.ops.array_ops) is deprecated and will be removed in a future version.\n",
      "Instructions for updating:\n",
      "Use tf.where in 2.0, which has the same broadcast rule as np.where\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "44/44 [==============================] - 414s 9s/step - loss: 0.4802 - accuracy: 0.8233 - val_loss: 0.5085 - val_accuracy: 0.9062\n",
      "Epoch 2/15\n",
      "44/44 [==============================] - 442s 10s/step - loss: 0.3783 - accuracy: 0.9635 - val_loss: 0.4394 - val_accuracy: 0.9375\n",
      "Epoch 3/15\n",
      "44/44 [==============================] - 775s 18s/step - loss: 0.3599 - accuracy: 0.9646 - val_loss: 0.4320 - val_accuracy: 0.9460\n",
      "Epoch 4/15\n",
      "44/44 [==============================] - 511s 12s/step - loss: 0.3445 - accuracy: 0.9724 - val_loss: 0.4036 - val_accuracy: 0.9517\n",
      "Epoch 5/15\n",
      "44/44 [==============================] - 475s 11s/step - loss: 0.3269 - accuracy: 0.9850 - val_loss: 0.3738 - val_accuracy: 0.9716\n",
      "Epoch 6/15\n",
      "33/44 [=====================>........] - ETA: 1:54 - loss: 0.3147 - accuracy: 0.9913"
     ]
    }
   ],
   "source": [
    "steps_per_epoch = train_generator.samples // train_generator.batch_size\n",
    "validation_steps = valid_generator.samples // valid_generator.batch_size\n",
    "hist = model.fit_generator(\n",
    "    train_generator,\n",
    "    epochs=15, steps_per_epoch=steps_per_epoch,\n",
    "    validation_data=valid_generator,\n",
    "    validation_steps=validation_steps).history"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "CYOw0fTO1W4x"
   },
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.ylabel(\"Loss (training and validation)\")\n",
    "plt.xlabel(\"Training Steps\")\n",
    "plt.ylim([0,2])\n",
    "plt.plot(hist[\"loss\"])\n",
    "plt.plot(hist[\"val_loss\"])\n",
    "\n",
    "plt.figure()\n",
    "plt.ylabel(\"Accuracy (training and validation)\")\n",
    "plt.xlabel(\"Training Steps\")\n",
    "plt.ylim([0,1])\n",
    "plt.plot(hist[\"accuracy\"])\n",
    "plt.plot(hist[\"val_accuracy\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "YCsAsQM1IRvA"
   },
   "source": [
    "Finally, the trained model can be saved for deployment to TF Serving or TF Lite (on mobile) as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "LGvTi69oIc2d"
   },
   "outputs": [],
   "source": [
    "saved_model_path = \"/Users/Travis/Programs/Air/python3/tensorflow2/transferlearning_image_classification/webcams/saved_model_epoc15_20200811\"\n",
    "tf.saved_model.save(model, saved_model_path)"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "\n",
    "Verifying the model\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "accelerator": "GPU",
  "colab": {
   "collapsed_sections": [
    "ScitaPqhKtuW"
   ],
   "last_runtime": {
    "build_target": "",
    "kind": "local"
   },
   "name": "TF Hub for TF2: Retraining an image classifier",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
