#!/usr/bin/env python
# coding: utf-8



import tensorflow as tf
import json
from config import *
import os
import PIL
import shutil
import datetime, time

def is_image(filename, verbose=False):

    data = open(filename,'rb').read(10)

    # check if file is JPG or JPEG
    if data[:3] == b'\xff\xd8\xff':
        if verbose == True:
             print(filename+" is: JPG/JPEG.")
        return True

    # check if file is PNG
    if data[:8] == b'\x89\x50\x4e\x47\x0d\x0a\x1a\x0a':
        if verbose == True:
             print(filename+" is: PNG.")
        return True

    # check if file is GIF
    if data[:6] in [b'\x47\x49\x46\x38\x37\x61', b'\x47\x49\x46\x38\x39\x61']:
        if verbose == True:
             print(filename+" is: GIF.")
        return True

    return False

def complete_image(filename):

    pilImg = PIL.Image.open(filename)

    try:
        pilImg.load()
        return True
    except IOError:
        return False


def evaluate():

    #if this is not an image file, remove it
    for file in os.listdir(imgsdir):
        if not is_image(imgsdir+'/'+file):
            os.remove(imgsdir+'/'+file)
            print (file,"is not an image")
    #if the image file is truncated, remove it
    for file in os.listdir(imgsdir):
        if not complete_image(imgsdir+'/'+file):
            os.remove(imgsdir+'/'+file)
            print (file," is truncated")  




    #modelname='../train/saved_model_org'
    #modelname='../train/saved_model_epoc10_20201201'
    modelname='../train/saved_model_epoc15_20200811'
    model = tf.keras.models.load_model(modelname)




    #preprocess images
    IMAGE_SIZE=(224,224)
    BATCH_SIZE = 32 #@param {type:"integer"}
    datagen_kwargs = dict(rescale=1./255)
    dataflow_kwargs = dict(target_size=IMAGE_SIZE, batch_size=BATCH_SIZE,
                    interpolation="bilinear")

    datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        **datagen_kwargs)
    train_generator = datagen.flow_from_directory(
        workdir, subset="training", shuffle=False, **dataflow_kwargs)



    #predict results
    train_generator.reset()
    pred=model.predict_generator(train_generator,verbose=1)



    labels = (train_generator.class_indices)
    labels = dict((v,k) for k,v in labels.items())
    filenames=train_generator.filenames




    arr=[]
    for i in range(len(pred)):
        print (filenames[i],pred[i,1])
        obj={
            'file':filenames[i],
            'fog':str(pred[i,1]),
        }
        arr.append(obj)
    print (arr)
    with open(NNresults, 'w') as outfile:
        json.dump(arr, outfile)

    #write time to timefile for webpage
    d = datetime.datetime.now()
    timeepoc = int(time.mktime(d.timetuple())) * 1000
    with open(timefile, 'w') as outfile:
        json.dump(timeepoc, outfile)

    #move the folder to from the workdir to the operational dir (outdir)
    if os.path.exists(outimgsdir):
        shutil.rmtree(outimgsdir)
    shutil.move(imgsdir, outimgsdir)


