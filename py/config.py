
import os

pydir=os.path.dirname(os.path.realpath(__file__))

workdir=pydir+'/workdir'
imgsdir=workdir+'/images'
NNresults=imgsdir+'/NNresults.json'
timefile=imgsdir+'/time.json'

outimgsdir=pydir+'/outdir/images'




def mkdir(dir):
    try:
        # Create target Directory
        os.makedirs(dir)
    except FileExistsError:
        dummy=0