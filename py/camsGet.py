
import os
import json
import requests
import sys
import datetime
from multiprocessing.pool import ThreadPool
from config import *
import shutil
import csv

def download_url(metadata):
  url        =metadata['description']
  outfilename=metadata['filename']

  try:
    r = requests.get(url, stream=True, timeout=10)  #timeout in 5 seconds if we can't get file
    if r.status_code == requests.codes.ok:
      with open(outfilename, 'wb') as f:
        for data in r:
          f.write(data)
      return metadata
  except:  #Catch all exceptions
    pass

def getcams():
  date1 = datetime.datetime.now()
  #check to see if the directory already exists
  if os.path.exists(imgsdir):
    #get date of dir
    st=os.stat(imgsdir)    
    mtime=st.st_mtime
    date2=datetime.datetime.fromtimestamp(mtime)
    #difference between now and folder date
    diff = date1 - date2
    days, seconds = diff.days, diff.seconds
    hours = days * 24 + seconds // 3600
    if hours > 1:
      print ("Work directory is old, deleting now")
      shutil.rmtree(imgsdir)
    else:
      print ("Work directory exists; exiting script")
      sys.exit()

  mkdir(imgsdir)

  #open csv and place into an array of dictionaries
  metadata=[]
  with open('../static/webcams.csv', mode='r') as f:
    reader = csv.DictReader(f)
    for row in reader:
      metadata.append(row)

  #edit the metadata to include a unique outfile name
  #A unique name is needed b/c many cams have the same name (i.e. cam.jpg)
  count=0
  for d in metadata:
    file_name_start_pos = d['description'].rfind("/") + 1
    file_name = imgsdir+'/'+str(count)+'_'+d['description'][file_name_start_pos:]
    metadata[count]['filename']=file_name
    count=count+1


  #down the files in the metadata
  #files that download successfully will be put into metadatagood
  metadatagood=[]
  download=True
  if download:
    # Run 5 multiple threads. Each call will take the next element in urls list
    results = ThreadPool(5).imap_unordered(download_url,metadata)
    for m in results:
      if m is not None:
        metadatagood.append(m)
        print("Finished Downloading",m['description'])

  #save this new metadata to file
  jsondata=json.dumps(metadatagood, indent=4)
  with open('../static/webcams.json', 'w') as f:
    f.write(jsondata)
    f.close()


if __name__ == "__main__":
  getcams()