
import os,sys
uname=os.uname()

try:
    uname=uname[1]
except:
    uname='none'

if uname == 'otx-ls-snow.geg.noaa.gov':
    sys.path.append("../env/lib/python3.5/site-packages/")

from camsGet import *
from camsEvaluate import *  


#download webcam images
getcams()
#evaluate webcams
evaluate()

